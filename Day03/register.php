<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân Sinh Viên</title>
    <style>
        body {
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;
        }

        h1 {
            color: #333;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            width: 90%;
            margin: 0 auto;
        }

        label[for="hoten"],
        label[for="phankhoa"],
        label[for="gender"] {
            font-weight: semibold;
            background-color: cornflowerblue;
            border: 2px solid royalblue ;
            border-radius: 3px;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: block;
            width: 30%; /* Điều chỉnh kích thước label tại đây */

        }

        .form-group {
            display: flex;
            align-items: center;
        }

        .form-group label {
            margin-right: 20px; /* Thêm khoảng cách giữa label và input/select */
        }

        input[type="text"],
        input[type="password"] {
            flex: 1;
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }

        .radio-group {
            display: flex;
            align-items: center;
        }

        /* Đặt màu chữ của "Nam" và "Nữ" */
        .gender-label {
            color: black; /* Màu chữ mặc định */
        }

        input[type="radio"] {
            margin-right: 5px;
            background-color: cornflowerblue;
        }

        select {
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #6B8E23;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            border: 1px solid cornflowerblue;
            cursor: pointer;
            font-weight: bold;
            margin: 20px auto;
            text-align: center;
            display: block;
        }
    </style>
</head>
<body>
    <h1>Đăng ký Tân Sinh Viên</h1>
    <form action="process_register.php" method="POST">
        <div class="form-group">
            <label for="hoten" class="custom-label">Họ và tên</label>
            <input type="text" id="hoten" name="hoten" class="custom-input" required />
        </div>

        <div class="form-group radio-group">
            <label for ="gender"> Giới tính </label>
            <input type="radio" id="nam" name="gioitinh" value="0" required>
            <label for="nam" class="gender-label">Nam</label>
            <input type="radio" id="nu" name="gioitinh" value="1" required>
            <label for="nu" class="gender-label">Nữ</label>
        </div>

        <div class="form-group">
            <label for="phankhoa" class="custom-label">Phân khoa</label>
            <select name="phankhoa" id="phankhoa" class="custom-input">
                <option value="">--Chọn phân khoa--</option>
                <option value="MAT">Khoa học máy tính</option>
                <option value="KDL">Khoa học vật liệu</option>
            </select>
        </div>

        <input type="submit" value="Đăng ký" />
    </form>
</body>
</html>