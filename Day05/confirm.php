<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation form </title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        $name = isset($_POST["name"]) ? $_POST["name"] : 'Không xác định';
        $gender = isset($_POST["gender"]) ? $_POST["gender"] : 'Không xác định';
        $department = isset($_POST["department"]) ? $_POST["department"] : 'Không xác định';
        $dob = isset($_POST["dob"]) ? $_POST["dob"] : 'Không xác định';
        $address = isset($_POST["address"]) ? $_POST["address"] : 'Không xác định';
        $image = isset($_FILES["image"]) && isset($_FILES["image"]["name"]) ? $_FILES["image"]["name"] : 'noimage.jpg';



        // Handle file upload
        $targetDirectory = "../Day05/uploads/";

        if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
            // Handle file upload
            $targetFile = $targetDirectory . basename($_FILES["image"]["name"]);

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile)) {
                // File moved successfully
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        } else {
            echo "No file was uploaded.";
        }
    }
    ?>
    <div class="form-container">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Họ và tên:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $name = isset($_POST["name"]) ? $_POST["name"] : 'Không xác định';
                        echo $name;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="gender">Giới tính:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $gender = isset($_POST["gender"]) ? $_POST["gender"] : 'Không xác định';
                        echo $gender;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="department">Phân khoa:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $department = isset($_POST["department"]) ? $_POST["department"] : 'Không xác định';
                        echo $department;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="dob">Ngày sinh:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $dob = isset($_POST["dob"]) ? $_POST["dob"] : 'Không xác định';
                        echo $dob;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="address">Địa chỉ:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $address = isset($_POST["address"]) ? $_POST["address"] : 'Không xác định';
                        echo $address;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="image">Hình ảnh:</label><br>
                <img src="../Day05/uploads/<?php echo $image; ?>" alt="Hình ảnh">

            </div>

            <input type="submit" value="Xác nhận" />
        </form>
    </div>
</body>

</html>