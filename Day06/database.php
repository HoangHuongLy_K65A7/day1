<?php
$servername = "localhost";
$username = "root"; // Thay 'username' bằng tài khoản MySQL của bạn
$password = ""; // Thay 'password' bằng mật khẩu của bạn
$dbname = "student_registration";

// Tạo kết nối
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Kiểm tra kết nối
if (!$conn) {
    die("Kết nối thất bại: " . mysqli_connect_error());
}

// Lấy dữ liệu từ form
// $name = $_POST['name'];
// $gender = $_POST["gender"];
// $department = $_POST["department"];
// $dob = $_POST["dob"];
// $address = $_POST["address"];
// $image = $_FILES["image"];

$name = "";
$gender = "";
$department = "";
$dob = "";
$address = "";
$image = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if (isset($_POST["name"])){
        $name = $_POST['name'];
    }
    if (isset($_POST["gender"])){
        $gender = $_POST["gender"];
    }
    if (isset($_POST["department"])){
        $department = $_POST["department"];
    }
    if (isset($_POST["dob"])){
        $dob = $_POST["dob"];
    }
    if (isset($_POST["address"])){
        $address = $_POST["address"];
    }
    if (isset($_FILES["image"])){
        $image = $_FILES["image"];
    }

}

// Thêm dữ liệu vào bảng
$sql = "INSERT INTO students (name, gender, department, dob, address,image_path)
        VALUES ('$name', '$gender', '$department', '$dob', '$address', '$image')";

if ($conn->query($sql) === TRUE) {
    echo "Dữ liệu đã được thêm thành công.";
} else {
    echo "Lỗi: " . $sql . "<br>" . $conn->error;
}

// Đóng kết nối
$conn->close();
?>
