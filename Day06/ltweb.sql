CREATE DATABASE student_registration;
USE student_registration;

CREATE TABLE students (
    name VARCHAR(50) NOT NULL,
    gender ENUM('Nam', 'Nữ', 'Khác') NOT NULL,
    department VARCHAR(50) NOT NULL,
    dob DATE NOT NULL,
    address VARCHAR(255) NOT NULL,
    image_path VARCHAR(255)


    
);