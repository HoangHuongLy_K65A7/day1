<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./style.css">
  <title>Registration Form</title>
  
</head>
<body>

    <div class="form-container">
    <form action="confirm.php" method="post" enctype="multipart/form-data" onsubmit="validateForm();">
    <div class="error-messages" id="errorMessages"></div>

      <div class="form-group">
        <label for="name">Họ và tên<span class="required">*</span></label>
        <input type="text" id="name" name="name">
      </div>

      <div class="form-group">
        <label for="gender">Giới tính<span class="required">*</span></label>
        <input type="radio" id="male" name="gender" value="Nam">
        <label for="male">Nam</label>
        <input type="radio" id="female" name="gender" value="Nữ">
        <label for="female">Nữ</label>
      </div>

      <div class="form-group">
        <label for="department">Phân khoa<span class="required">*</span></label>
        <select id="department" name="department">
          <option value="">—Chọn phân khoa—</option>
          <option value="MAT">Khoa học máy tính</option>
          <option value="KDL">Khoa học vật liệu</option>
        </select>
      </div>

      <div class="form-group">
        <label for="dob">Ngày sinh<span class="required">*</span></label>
        <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
      </div>
      <div class="form-group">
        <label for="address">Địa chỉ<span class="required">*</span></label>
        <input type="text" id="address" name="address">
      </div>
      <div class="form-group">
      <label for="image">Hình ảnh</label><br>
      <input type="file" id="image" name="image" accept=".png, .jpg, .jpeg">

    </div>
        
      <input type="submit" value="Đăng ký" />
    </form>

  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="./script.js"></script>
</body>
</html>

