function validateForm() {
    const errorMessages = [];
    const name = document.getElementById("name").value;
    const gender = document.querySelector('input[name="gender"]:checked');
    const department = document.getElementById("department").value;
    const dob = document.getElementById("dob").value;
    
    if (!name) {
      errorMessages.push("Hãy nhập tên.");
    }
    
    if (!gender) {
      errorMessages.push("Hãy chọn giới tính.");
    }
    
    if (!department) {
      errorMessages.push("Hãy chọn phân khoa.");
    }
    
    if (!dob) {
      errorMessages.push("Hãy nhập ngày sinh.");
    } else {
      const datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
      if (!datePattern.test(dob)) {
        errorMessages.push("Hãy nhập ngày sinh đúng định dạng (dd/mm/yyyy).");
      }
    }
  
    displayErrorMessages(errorMessages);
  }
  
  function displayErrorMessages(messages) {
    const errorMessagesDiv = document.getElementById("errorMessages");
    errorMessagesDiv.innerHTML = "";
    
    if (messages.length > 0) {
      const ul = document.createElement("ul");
      messages.forEach((message) => {
        const li = document.createElement("li");
        li.textContent = message;
        ul.appendChild(li);
      });
      errorMessagesDiv.appendChild(ul);
    }
  }
  