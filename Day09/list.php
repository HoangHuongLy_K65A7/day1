<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link rel="stylesheet" href="./styles/list_style.css">
</head>
<body>
<div class="container">
        <form action="">
            <label for="inputname" class="input_name">Khoa</label>
            <input type="text" id="input_name" name="inputname" class="entering" required><br><br>

            <label for="inputname" class="input_name">Từ Khóa </label>
            <input type="text" id="tu_khoa" name="inputname" class="entering" required><br><br>
        </form>

        <button class="button-container" id="nonsubmitButton" > Reset </button>

        <form action="register.php">
            <button class="button-container" id="submitButton"> Thêm </button>
        </form>
        
        <p class="tittle">Số sinh viên tìm thấy : XXX</p>
        <div>
            <table>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);


                include 'database.php';

                $sql = "SELECT full_name, faculty, id FROM STUDENTS";
                $result = $conn->query($sql);

                $i = 1 ;
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>" . $i . "</td>";
                    echo "<td>" . $row["full_name"]. "</td>";
                    echo "<td>" . $row["faculty"]. "</td>";
                    echo "<td>";
                    echo '<button class="button-container delete-button" data-student-id="' .$row["id"]. '">Xóa</button>' ;
                    echo '<button class="button-container edit-button" data-student-id="' .$row["id"]. '">Sửa</button>' ;
                    echo "</td>";
                    echo "</tr>";
                    $i++;
                }

                $conn->close();
                ?>
                </tbody>
            </table>
        </div>
        <script type="text/javascript" src="./scripts/list_handle.js"></script>
    </div>
</body>
</html>