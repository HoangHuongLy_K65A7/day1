<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dăng ký</title>
    <link rel="stylesheet" href="./styles/register_style.css">
</head>
<body>
    <div class="container">
        <!-- -->
        <div id="errorMessage"></div>

        <!--  -->
        <form action="confirm.php" method="post" enctype="multipart/form-data">

            <!--  -->
            <div class="form-element">
                <label for="inputname" class="input_name"> Họ và tên <span class="required">*</span></label>
                <input type="text" name="inputname" class="entering" required>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="gioitinh" class="input_name input_gen">Giới tính <span class="required">*</span></label>
                <div class="choose_gender">
                    <label for="male">
                        <input type="radio" id="male" name="gender" value="Nam"> Nam
                    </label>
                    <label for="female">
                        <input type="radio" id="female" name="gender" value="Nữ"> Nữ
                    </label>
                </div>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="phankhoa" class="input_name input_falcuty">Phân khoa <span class="required">*</span></label>
                <select name="phankhoa" class="choose-falcuty" required>
                    <option value="">-- Chọn phân khoa --</option>
                    <option value="KHMT">Khoa học máy tính </option>
                    <option value="KHDL">Khoa học dữ liệu </option>
                </select> 
            </div>

            <!--  -->
            <div class="form-element">
                <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh <span class="required">*</span></label>
                <input type="text" id="dob" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" required>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="address" class="input_name address"> Địa chỉ </label>
                <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
            </div>
            
            <!--  -->
            <div class="form-element">
                <label for="image" class="input_name uploadImage"> Hình ảnh </label>
                <input id="image" type="file" name="fileToUpload" class="fileToUpload">
            </div>

            <!--  -->
            <div class="button-submit">
                <button type="submit" class="button-container" id="submitButton"> Đăng ký </button>
            </div>

        </form>

        <script type="text/javascript" src="./scripts/register.js"></script>

    </div>
</body>
</html>