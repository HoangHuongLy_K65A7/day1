<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form đăng kí sinh viên</title>
    <style>
        body {
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;
        }

        h1 {
            color: #333;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            width: 90%;
            margin: 0 auto;
        }

        label[for="name"],
        label[for="gender"],
        label[for="dob"],
        label[for="address"],
        label[for="thongtinkhac"] {
            font-weight: semibold;
            background-color: cornflowerblue;
            border: 2px solid royalblue ;
            border-radius: 3px;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: block;
            width: 30%; /* Điều chỉnh kích thước label tại đây */

        }

        .form-group {
            display: flex;
            align-items: center;
        }

        .form-group label {
            margin-right: 20px; /* Thêm khoảng cách giữa label và input/select */
        }

        input[type="text"],
        input[type="password"] {
            flex: 1;
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }

        .radio-group {
            display: flex;
            align-items: center;
        }

        /* Đặt màu chữ của "Nam" và "Nữ" */
        .gender-label {
            color: black; /* Màu chữ mặc định */
        }

        input[type="radio"] {
            margin-right: 5px;
            background-color: cornflowerblue;
        }

        select {
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #6B8E23;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            border: 1px solid cornflowerblue;
            cursor: pointer;
            font-weight: bold;
            margin: 20px auto;
            text-align: center;
            display: block;
        }
    </style>
</head>
<body>

    <h1>Form đăng kí sinh viên</h1>
    <form action="process_register.php" method="POST">
        <div class="form-container">
        <form id="registrationForm" onsubmit="validateForm(); return false;">
        <div class="error-messages" id="errorMessages"></div>

        <div class="form-group">
            <label for="name" class="custom-label">Họ và tên</label>
            <input type="text" id="name" name="name" class="custom-input" required />
        </div>

        <div class="form-group radio-group">
            <label for ="gender"> Giới tính </label>
            <input type="radio" id="nam" name="gender" value="1" required>
            <label for="nam" class="gender-label">Nam</label>
            <input type="radio" id="nu" name="gender" value="2" required>
            <label for="nu" class="gender-label">Nữ</label>
        </div>

        <div class="form-group">
        <label for="dob">Ngày Sinh:</label>
        <input type="date" name="dob" min="YEAR-40-01" max="YEAR-15-12" required><br><br>
        </div>

      <div class="form-group">
      <label for="address">Địa chỉ:</label>
        <select id="thanh_pho" name="thanh_pho" required>
            <option value="">Chọn thành phố</option>
            <option value="Hanoi">Hà Nội</option>
            <option value="TPHCM">Thành phố Hồ Chí Minh</option>
        </select><br><br>

        <label for="quan">Quận:</label>
        <select id="quan" name="quan" required>
            <option value="">Chọn quận</option>
        </select><br><br>
      </div>
      
     
      <div class="form-group">
        <label for="thongtinkhac">Thông tin khác<span class="N/A"></span></label>
        <textarea name="thongtinkhac" rows="4" cols="50"></textarea><br><br>
      </div>
      

        <input type="submit" value="Đăng ký" />
    </form>
</body>
</html>