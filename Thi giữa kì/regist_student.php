<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation form </title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        $name = isset($_POST["name"]) ? $_POST["name"] : 'Không xác định';
        $gender = isset($_POST["gender"]) ? $_POST["gender"] : 'Không xác định';
        $dob = isset($_POST["dob"]) ? $_POST["dob"] : 'Không xác định';
        $address = isset($_POST["address"]) ? $_POST["address"] : 'Không xác định';
        
    }
    ?>
    <div class="form-container">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Họ và tên:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $name = isset($_POST["name"]) ? $_POST["name"] : 'Không xác định';
                        echo $name;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="gender">Giới tính:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $gender = isset($_POST["gender"]) ? $_POST["gender"] : 'Không xác định';
                        echo $gender;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

        

            <div class="form-group">
                <label for="dob">Ngày sinh:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $dob = isset($_POST["dob"]) ? $_POST["dob"] : 'Không xác định';
                        echo $dob;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="address">Địa chỉ:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $address = isset($_POST["address"]) ? $_POST["address"] : 'Không xác định';
                        echo $address;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

            <div class="form-group">
                <label for="thongtinkhac">Thông tin khác:</label>
                <span>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $thongtinkhac = isset($_POST["thongtinkhac"]) ? $_POST["thongtinkhac"] : 'Không xác định';
                        echo $thongtinkhac;
                    } else {
                        echo 'Không có thông tin đăng ký để xác nhận.';
                    }
                    ?>
                </span>
            </div>

           
            <input type="submit" value="Xác nhận" />
        </form>
    </div>
</body>

</html>