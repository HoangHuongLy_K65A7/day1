
    var ngaySinhInput = document.querySelector('input[name="ngay_sinh"]');
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var minYear = currentYear - 40;
    var maxYear = currentYear - 15;
    var minDate = `${minYear}-01-01`;
    var maxDate = `${maxYear}-12-31`;

    ngaySinhInput.setAttribute('min', minDate);
    ngaySinhInput.setAttribute('max', maxDate);


        var thanhPhoSelect = document.getElementById("thanh_pho");
        var quanSelect = document.getElementById("quan");

        // Bộ dữ liệu cho các quận dựa trên thành phố
        var quanData = {
            Hanoi: ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
            TPHCM: ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]
        };

        thanhPhoSelect.addEventListener("change", function () {
            var selectedCity = thanhPhoSelect.value;
            var quanArray = quanData[selectedCity] || [];
            quanSelect.innerHTML = '<option value="">Chọn quận</option>';

            for (var i = 0; i < quanArray.length; i++) {
                var option = document.createElement("option");
                option.text = quanArray[i];
                option.value = quanArray[i];
                quanSelect.appendChild(option);
            }
        });

    
    function validateForm() {
    const errorMessages = [];
    const name = document.getElementById("name").value;
    const gender = document.querySelector('input[name="gender"]:checked');
    const dob = document.getElementById("dob").value;
    
    if (!name) {
      errorMessages.push("Hãy nhập họ tên.");
    }
    
    if (!gender) {
      errorMessages.push("Hãy chọn giới tính.");
    }
    
    
    if (!dob) {
      errorMessages.push("Hãy chọn ngày sinh.");
    }

    if (!address) {
        errorMessages.push("Hãy chọn địa chỉ.");
      }
  
    displayErrorMessages(errorMessages);
  }
  
  function displayErrorMessages(messages) {
    const errorMessagesDiv = document.getElementById("errorMessages");
    errorMessagesDiv.innerHTML = "";
    
    if (messages.length > 0) {
      const ul = document.createElement("ul");
      messages.forEach((message) => {
        const li = document.createElement("li");
        li.textContent = message;
        ul.appendChild(li);
      });
      errorMessagesDiv.appendChild(ul);
    }
  }
  